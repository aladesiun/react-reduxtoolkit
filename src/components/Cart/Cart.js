import Card from '../UI/Card';
import classes from './Cart.module.css';
import CartItem from './CartItem';
import { useSelector } from 'react-redux';

const Cart = (props) => {
  const cartItems = useSelector(state => state.cart.items);
  const prices = [];
  cartItems.map((item) => prices.push(item.totalPrice));
  return (
    <Card className={classes.cart}>
      <h2>Your Shopping Cart</h2>
      <ul>
        {cartItems.map((item, idx) => (
          <CartItem
            key={idx}
            item={{ title:item.name, quantity: item.quantity, total: item.totalPrice, price: item.price, id:item.id }}
          />
        ))}

      </ul>
      <h2>Total Price: ${prices ? prices.reduce((partialSum, currentValue) => partialSum + currentValue, 0): ''}</h2>

    </Card>
  );
};  

export default Cart;
