import { uiAction } from "./ui-slice";
import { cartActions } from "./cart-slice";
export const sendCartData = (cart) => {
    return async (dispatch) => {
        dispatch(uiAction.showNotification(
            {
                status: 'pending',
                title: 'sending....',
                message: 'sending cart data!',
            }
        ))
        const sendRequest = async () => {
            const response = await fetch('https://redux-toolkit-af908-default-rtdb.firebaseio.com/cart.json',
                {
                    method: 'PUT',
                    body: JSON.stringify(cart),
                });
            if (!response.ok) {
                throw new Error('Sending cart data failed')
            }

        };
        try {
            await sendRequest();
            dispatch(uiAction.showNotification(
                {
                    status: 'success',
                    title: 'success!',
                    message: 'sent cart data successfully',
                }
            ))
        }
        catch (error) {
            dispatch(uiAction.showNotification(
                {
                    status: 'error',
                    title: 'Error!',
                    message: 'sent cart data failed',
                }
            ))
        }
    }
}
export const fetchCartData = () => {
    return async (dispatch) => {

        const fetchData = async () => {
            const response = await fetch('https://redux-toolkit-af908-default-rtdb.firebaseio.com/cart.json');
            if (!response) {
                throw new Error('Could not fetch cart data!');
            }
            const data = await response.json();
            return data;
        }

        try {
            const cartData = await fetchData();
            dispatch(cartActions.replaceCart(
                {
                    items:cartData.items ?? [],
                    totalQuantity:cartData.totalQuantity ?? 0 
                }))
        } catch (error) {
            console.log(error);
            dispatch(uiAction.showNotification(
                {
                    status: 'error',
                    title: 'Error!',
                    message: error.message,
                }
            ))
        }
    }
} 